this is a wallpaper changer and manager for plasmawayland tested in debian linux

it uses the Pixabay API for downloading photos for your wallpaper you'll need to create an api key 
to create a api key head over to https://pixabay.com/ click on join then create an account 
take your api key in the download folder create a file named key.py then and the file add 
ApiKey = "your api key" and save the file

once your api key is set up in the main folder run the wallpaper.sh script
at first it will look if the photos are already downloaded if not it will start downloading them 
it will change your wallpaper every minute and keep track at the time of day and time of year
in a winter night it will use winter night wallpapers and at sunrise change to winter sunrise views
and so on 

this is the first deployment of the project and i'll work to make it even better

the Qu@ntumM3n7$ch
