import datetime
from datetime import datetime
import ImageDownloader
import os


month = datetime.now().month
hour = datetime.now().hour


winter=[12,1,2,]
spring=[3,4,5]
summer=[6,7,8]
fall=[9,10,11]


if month in winter:
    season = "winter"
elif month in spring:
    season = "spring"
elif month in summer:
    season = "summer"
else:
    season = "fall"

if hour < 5 or hour > 19:
    time = "night"
elif hour < 9:
    time = "sunrise"
elif hour < 16:
    time = "day"
else:
    time = "sunset"

swd = "/home/tuli/.wallpaper"
PathForFile = swd + "/" + season + "/" + time

total=0
Wallpaper=False

if os.path.exists(PathForFile):
    for image in os.listdir(PathForFile):
        if image.lower().endswith('.jpg'):
            total+=1
            if total >= 10:
                Wallpaper = True

if Wallpaper == False:
    ImageDownloader.run(swd + "/" + season, season)

print(PathForFile)
