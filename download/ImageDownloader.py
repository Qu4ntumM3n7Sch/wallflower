import requests
import os
import key
import time

def run(path, season):
    times = ["night", "sunrise", "day", "sunset"]

    for TheTime in times:
        ThePath = path + "/" + TheTime
        if not os.path.exists(ThePath):
            os.makedirs(ThePath)
        ToSearch = f"{season}+{TheTime}+landscape"
        url = f'https://pixabay.com/api/?key={key.ApiKey}&q={ToSearch}&image_type=photo&orientation="horizontal"&pretty=true'

        response = requests.get(url)
        JsonData = response.json()
        for image in JsonData['hits']:
            name = image['id']
            ImgUrl = image['largeImageURL']
            response = requests.get(ImgUrl, stream=True)
            with open(ThePath + "/" + str(name) + '.jpg', 'wb') as f:
                f.write(response.content)
        time.sleep(10)
